#pragma once

constexpr double squaredSum(double a, double b) noexcept
{
	return (a + b) * (a + b);
}